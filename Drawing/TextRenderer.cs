﻿using System;
using System.Drawing;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace YSU_Metro.Drawing
{
    public class TextRenderer : IDisposable
    {
        public int Width { get; private set; }
        public int Height { get; private set; }
        public int Texture
        {
            get
            {
                UploadBitmap();
                return _texture;
            }
        }

        private readonly Bitmap _bitmap;
        private readonly Graphics _graphics;
        private readonly int _texture;
        private Rectangle _dirtyRegion;

        public TextRenderer(int width, int height)
        {
            Width = width;
            Height = height;
            _bitmap = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            _graphics = Graphics.FromImage(_bitmap);
            _graphics.Clear(Color.Transparent);
            _graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAlias;
            _dirtyRegion = new Rectangle(0, 0, _bitmap.Width, _bitmap.Height);
            _texture = GL.GenTexture();
            GL.BindTexture(TextureTarget.Texture2D, _texture);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Linear);
            GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Linear);
            GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgba, width, height, 0,
                PixelFormat.Rgba, PixelType.UnsignedByte, IntPtr.Zero);
        }

        public void DrawString(string text, Font font, Brush brush, PointD point)
        {
            _graphics.DrawString(text, font, brush, new PointF((float)point.X, (float)point.Y));

            SizeF size = _graphics.MeasureString(text, font);
            _dirtyRegion = Rectangle.Round(RectangleF.Union(_dirtyRegion, new RectangleF(PointF.Empty, size)));
            _dirtyRegion = Rectangle.Intersect(_dirtyRegion, new Rectangle(0, 0, _bitmap.Width, _bitmap.Height));
        }

        private void UploadBitmap()
        {
            if (_dirtyRegion == RectangleF.Empty) return;

            System.Drawing.Imaging.BitmapData data = _bitmap.LockBits(_dirtyRegion,
                System.Drawing.Imaging.ImageLockMode.ReadOnly,
                System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            GL.BindTexture(TextureTarget.Texture2D, _texture);
            GL.TexSubImage2D(TextureTarget.Texture2D, 0,
                _dirtyRegion.X, _dirtyRegion.Y, _dirtyRegion.Width, _dirtyRegion.Height,
                PixelFormat.Bgra, PixelType.UnsignedByte, data.Scan0);

            _bitmap.UnlockBits(data);
            _dirtyRegion = Rectangle.Empty;
        }

        public void Dispose()
        {
            _bitmap.Dispose();
            _graphics.Dispose();
            if (GraphicsContext.CurrentContext != null)
                GL.DeleteTexture(_texture);
        }
    }
}