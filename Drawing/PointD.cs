﻿using System;

namespace YSU_Metro.Drawing
{
    [Serializable]
    public struct PointD
    {
        public double X { get; set; }
        public double Y { get; set;}

        public static PointD Empty
        {
            get { return new PointD(0, 0); }
        }

        public PointD(double x, double y) : this()
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object obj)
        {
            return obj is PointD && this == (PointD)obj;
        }
        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }
        public static bool operator ==(PointD a, PointD b)
        {
            return a.X == b.X && a.Y == b.Y;
        }
        public static bool operator !=(PointD a, PointD b)
        {
            return !(a == b);
        }

        public static PointD operator +(PointD a, PointD b)
        {
            return new PointD(a.X + b.X, a.Y + b.Y);
        }

        public static PointD operator -(PointD a, PointD b)
        {
            return new PointD(a.X - b.X, a.Y - b.Y);
        }
    }
}