﻿using System;

namespace YSU_Metro.Drawing
{
    [Serializable]
    public class TextStyle
    {
        public PointD Margin { get; private set; }

        public static TextStyle Default
        {
            get { return new TextStyle(new PointD(8, -10)); }
        }
        public TextStyle(PointD margin)
        {
            Margin = margin;
        }
    }
}