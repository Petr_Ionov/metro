﻿using YSU_Metro.Drawing;
using SMath = System.Math;

namespace YSU_Metro.Utilities
{
    public static class Math
    {
        public static double VectorLength(PointD startPoint, PointD endPoint)
        {
            return SMath.Sqrt(
                SMath.Pow(endPoint.X - startPoint.X, 2) + SMath.Pow(endPoint.Y - startPoint.Y, 2)
                );
        }

        public static PointD VectorCoords(PointD startPoint, PointD endPoint)
        {
            return new PointD(endPoint.X - startPoint.X, endPoint.Y - startPoint.Y);
        }

        public static PointD PointCoordsOnVector(PointD startPoint, PointD endPoint, double ratio)
        {
            return new PointD(
                (startPoint.X + ratio * VectorCoords(startPoint, endPoint).X),
                (startPoint.Y + ratio * VectorCoords(startPoint, endPoint).Y)
                );
            
        }

        public static PointD OrthogonalVector (PointD startPoint, PointD endPoint)
        {
            PointD vector = VectorCoords(startPoint, endPoint);

            if (vector.Y != 0d)
                return new PointD(1d, -vector.X / vector.Y);
            else
                return new PointD(0d, 1d);

        }

        public static double AngleBetweenVectorAndXAxis(PointD vectorCoords)
        {
            PointD xVector = new PointD(1, 0);
            double scalar = vectorCoords.X * xVector.X + vectorCoords.Y * xVector.Y;
            double moduleX = SMath.Sqrt(
                SMath.Pow(xVector.X, 2) + SMath.Pow(xVector.Y, 2)
                );
            double moduleV = SMath.Sqrt(
                SMath.Pow(vectorCoords.X, 2) + SMath.Pow(vectorCoords.Y, 2)
                );

            return SMath.Acos(scalar / (moduleX * moduleV));
        }
    }
}
