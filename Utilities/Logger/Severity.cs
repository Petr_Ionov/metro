﻿namespace YSU_Metro.Utilities.Logger
{
    public enum Severity
    {
        Info,
        Warning,
        Critical
    }
}