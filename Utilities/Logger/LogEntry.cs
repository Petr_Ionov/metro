﻿using System;

namespace YSU_Metro.Utilities.Logger
{
    public class LogEntry
    {
        public DateTime DateTime { get; private set; }
        public Severity Severity { get; private set; }
        public string Message { get; private set; }

        public LogEntry(string message, DateTime dateTime, Severity severity = Severity.Info)
        {
            Message = message;
            DateTime = dateTime;
            Severity = severity;
        }

        public LogEntry(string message, Severity severity = Severity.Info) 
            : this(message, System.DateTime.Now, severity) { }

        public override string ToString()
        {
            return string.Format("[{0}] {1}", DateTime.ToUniversalTime(), Message);
        }

        public string ToString(string dateMask)
        {
            return string.Format("[{0}] {1}", DateTime.ToString(dateMask), Message);
        }
    }
}