﻿using System;
using System.Collections.Generic;

namespace YSU_Metro.Utilities.Logger
{
    public class Logger : ILogger
    {
        private readonly List<LogEntry> _logEntries;

        public delegate void NewLogEntryHandler(LogEntry logEntry);
        public event NewLogEntryHandler OnNewLogEntry;

        public Logger()
        {
            _logEntries = new List<LogEntry>();
        }

        public void Write(LogEntry logEntry)
        {
            _logEntries.Add(logEntry);

            if (OnNewLogEntry != null)
            {
                OnNewLogEntry(logEntry);
            }
        }

        public void Write(string message, DateTime dateTime, Severity severity = Severity.Info)
        {
            Write(new LogEntry(message, dateTime, severity));
        }

        public IEnumerable<LogEntry> GetLogEntries()
        {
            return _logEntries;
        }

        public void Clear()
        {
            _logEntries.Clear();
        }
    }
}