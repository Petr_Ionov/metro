﻿using System;
using System.Collections.Generic;

namespace YSU_Metro.Utilities.Logger
{
    public interface ILogger
    {
        void Write(LogEntry logEntry);
        void Write(string message, DateTime dateTime, Severity severity = Severity.Info);
        IEnumerable<LogEntry> GetLogEntries();
        void Clear();
    }
}
