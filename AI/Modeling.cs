﻿using System;
using System.Timers;
using YSU_Metro.Enums;
using YSU_Metro.Models;
using YSU_Metro.Utilities.Logger;

namespace YSU_Metro.AI
{
    public class Modeling
    {
        private readonly Timer _timer;
        private ILogger _logger;
        private readonly ISituation _situation;
        private ModelingReport _report;
        public delegate void UpdateTrainsPositionHandler();
        public event UpdateTrainsPositionHandler TrainPositionUpdated;

        public delegate void EndedModeligHangler();
        public event EndedModeligHangler ModelingEnded;

        public ModelingSettings Settings { get; private set; }
        public ModelingState State { get; private set; }
        public DateTime CurrentTime { get; private set; }

        public Modeling(ModelingSettings settings)
        {
            Settings = settings;
            CurrentTime = new DateTime();
            _situation = new UnexpectedSituation(Settings.DayOfWeek);
            _timer = new Timer(1000);
            _timer.Elapsed += _timer_Elapsed;
            State = ModelingState.NotRunning;
        }

        public Modeling() : this(new ModelingSettings()) { }

        public void Start()
        {
            _timer.Start();
            CurrentTime = Settings.StartTime;
            _logger.Clear();
            State = ModelingState.Running;
            RunTrainsMovement();
        }

        public void Pause()
        {
            _timer.Stop();
            State = ModelingState.Pause;
        }

        public void Resume()
        {
            _timer.Start();
            State = ModelingState.Running;
        }

        public void Stop()
        {
            _timer.Stop();

            GenerateReport();

            //устанавливаем значения по умолчанию для поездов
            foreach (Train train in Settings.Trains)
            {
                train.SetDefaultValues();
            }
            CurrentTime = Settings.StartTime;

            State = ModelingState.NotRunning;

            if (ModelingEnded != null)
            {
                ModelingEnded();
            }
        }

        private void GenerateReport()
        {
            _report = new ModelingReport()
            {
                DayOfWeek = Settings.DayOfWeek,
                StartTime = Settings.StartTime,
                FinishTime = CurrentTime,
                Branch = Settings.Branch,
                Step = Settings.Step,
                LogEntries = _logger.GetLogEntries(),
                Trains = Settings.Trains
            };
        }

        public ModelingReport GetReport()
        {
            return _report;
        }

        public void InitLogger(ILogger logger)
        {
            _logger = logger;
        }

        private void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (CurrentTime >= Settings.FinishTime)
            {
                Stop();
                return;
            }
            ChangeTrainsCoords();
            CurrentTime = CurrentTime.AddSeconds(Settings.Step);
            if (TrainPositionUpdated != null)
            {
                TrainPositionUpdated(); 
            }
        }

        void EstablishPrevioursTrainsWihoutFirstTrains()
        {
            for (var i = 0; i < Settings.Trains.Count; i++)
                if (Settings.Trains[i].PreviuosTrain == null)
                    for (var j = i - 1; j >= 0; j--)
                        if (Settings.Trains[j].Direction == Settings.Trains[i].Direction)
                        {
                            Settings.Trains[i].PreviuosTrain = Settings.Trains[j];
                            break;
                        }
        }

        void EstablishPrevioursTrains()
        {
            TrainDirection CurrentDirection = Settings.Trains[0].Direction;
            int Count = Settings.Trains.Count;
            foreach (Train train in Settings.Trains)
                train.PreviuosTrain = null;

            //Предыдущий поезд для самого первого - последний поезд, едящий в другом направлении.
            for (var i = Count - 1; i >= 0; i--)
                if (Settings.Trains[i].Direction != CurrentDirection)
                {
                    Settings.Trains[0].PreviuosTrain = Settings.Trains[i];
                    break;
                }
            if (Settings.Trains[0].PreviuosTrain == null)
            {
                Settings.Trains[0].PreviuosTrain = Settings.Trains[Count - 1];
                EstablishPrevioursTrainsWihoutFirstTrains();
                return;
            }
            //Предыдущий поезд для первого поезда не текущего направления - последний в списке поезд текущего направления.
            for (var i = 0; i < Settings.Trains.Count; i++)
                if (Settings.Trains[i].Direction != CurrentDirection)
                {
                    for (var j = Count - 1; j >= 0; j--)
                        if (Settings.Trains[j].Direction == CurrentDirection)
                        {
                            Settings.Trains[i].PreviuosTrain = Settings.Trains[j];
                            break;
                        }
                    break;
                }
            EstablishPrevioursTrainsWihoutFirstTrains();
        }

        private int GetIndexOfStation(Station station)
        {
            return Settings.Branch.Stations.IndexOf(station);
        }

        private int GetIndexOfSection(Section section)
        {
            return Settings.Branch.Sections.IndexOf(section);
        }

        double GetAngle(Train train)
        {
            if (train.CurrentSection == null)
                return 0d;

            return YSU_Metro.Utilities.Math.AngleBetweenVectorAndXAxis(
                    YSU_Metro.Utilities.Math.OrthogonalVector(
                        train.CurrentSection.From.Coord, train.CurrentSection.To.Coord
                        )
                    );
        }

        void InitializationTrainsFields()
        {
            if (Settings.Trains.Count == 0)
            {
                return;
            }

            foreach (Train train in Settings.Trains)
            {
                train.Speed = 0d;
                train.CurrentDistanse = 0d;
                train.CurrentStation = train.StartStation;
                train.Coord = train.StartStation.Coord;
                train.Angle = 0d;
                train.CurrentDistanse = 0d;
                train.RemainingTimeStop = 0d;
                train.State = TrainState.Parking;

                int indOfStartStation = GetIndexOfStation(train.StartStation);
                if (train.StartStation == Settings.Branch.Stations[0] ||
                    Settings.Branch.Stations[indOfStartStation - 1].State != AvailableState.Working)

                    train.Direction = TrainDirection.Direct;
                else
                    train.Direction = TrainDirection.Opposite;

                for (var i = 0; i < Settings.Branch.Sections.Count; i++)
                    if (train.Direction == TrainDirection.Direct && train.StartStation == Settings.Branch.Sections[i].From ||
                        train.Direction == TrainDirection.Opposite && train.StartStation == Settings.Branch.Sections[i].To)
                    {
                        train.CurrentSection = Settings.Branch.Sections[i];
                        break;
                    }
            }

            EstablishPrevioursTrains();
        }

        void InitializationMovingTrain(Train train)
        {
            train.Speed = 1d;
            train.State = TrainState.Moving;
            train.CurrentStation = null;
            train.Angle = GetAngle(train);
            _logger.Write(string.Format("Поезд №{0} отбыл со станции {1}", train.Id, train.StartStation.Name), CurrentTime);
        }

        public void RunTrainsMovement()
        {
            bool findTrainDirectDirection = false, findTrainOppositeDirection = false;
            InitializationTrainsFields();

            foreach (Train train in Settings.Trains)
            {
                if (!findTrainDirectDirection && train.Direction == TrainDirection.Direct)
                {
                    findTrainDirectDirection = true;
                    InitializationMovingTrain(train);
                }
                else if (!findTrainOppositeDirection && train.Direction == TrainDirection.Opposite)
                {
                    findTrainOppositeDirection = true;
                    InitializationMovingTrain(train);
                }
            }
        }

        double GetSpeed(Train train)
        {
            double speed = 1d;
            if (train.CurrentSection.Time > Settings.Branch.Schedule.GetInterval(Settings.DayOfWeek, CurrentTime))
            {
                speed = train.CurrentSection.Time / Settings.Branch.Schedule.GetInterval(Settings.DayOfWeek, CurrentTime);
                if (speed > 1.5)
                    speed = 1.5;
            }
            return speed;
        }

        Section GetSection(Train train)
        {
            int indOfSection = GetIndexOfSection(train.CurrentSection);
            if (train.Direction == TrainDirection.Direct && indOfSection < Settings.Branch.Sections.Count - 1)
                return Settings.Branch.Sections[indOfSection + 1];
            if (train.Direction == TrainDirection.Opposite && indOfSection > 0)
                return this.Settings.Branch.Sections[indOfSection - 1];

            return null;
        }

        void EstablishTransFieldsAfterStopping(Train train)
        {
            bool isDirectionChanged = false;

            if (train.Direction == TrainDirection.Direct)
            {
                train.CurrentStation = train.CurrentSection.To;
                train.Coord = train.CurrentStation.Coord;
                int indOfStation = GetIndexOfStation(train.CurrentStation);
                if (indOfStation == Settings.Branch.Stations.Count - 1 || Settings.Branch.Stations[indOfStation + 1].State != AvailableState.Working)
                {
                    train.Direction = TrainDirection.Opposite;
                    isDirectionChanged = true;
                }
            }
            else
            {
                train.CurrentStation = train.CurrentSection.From;
                train.Coord = train.CurrentStation.Coord;
                int indOfStation = GetIndexOfStation(train.CurrentStation);
                if (indOfStation == 0 || Settings.Branch.Stations[indOfStation - 1].State != AvailableState.Working)
                {
                    train.Direction = TrainDirection.Direct;
                    isDirectionChanged = true;
                }
            }

            _logger.Write(string.Format("Поезд №{0} прибыл на станцию {1}", train.Id, train.CurrentStation.Name), CurrentTime);

            int delay = _situation.GetUnexpectedDelay(CurrentTime).Delay;
            if (delay > 0)
            {
                _logger.Write(string.Format("Поезд №{0} задерживается на {1} сек. на станции {2}", train.Id, delay,
                    train.CurrentStation.Name), CurrentTime);
            }

            if (!isDirectionChanged)
                train.CurrentSection = GetSection(train);
            train.Speed = 0d;
            train.State = TrainState.Stopping;
            train.Angle = 0d;
            train.CurrentDistanse = 0d;
            train.RemainingTimeStop = Settings.Branch.Schedule.GetStay(Settings.DayOfWeek, CurrentTime) + delay;
        }

        void EstablishTransFieldsBeforeStarting(Train train)
        {
            train.Speed = GetSpeed(train);
            train.State = TrainState.Moving;

            _logger.Write(string.Format("Поезд №{0} отбыл со станции {1}", train.Id, train.CurrentStation.Name), CurrentTime);

            train.CurrentStation = null;
            train.Angle = GetAngle(train);
            train.CurrentDistanse = 0d;
            train.RemainingTimeStop = 0d;

        }

        private bool IsStationFinal (Station station)
        {
            int count = Settings.Branch.Stations.Count;
            int ind = GetIndexOfStation(station);

            if (ind == 0 || ind == count - 1 ||
                Settings.Branch.Stations[ind - 1].State != AvailableState.Working ||
                Settings.Branch.Stations[ind + 1].State != AvailableState.Working)
                return true;
            return false;
        }

        public void ChangeTrainsCoords()
        {
            foreach (Train train in Settings.Trains)
            {
                if (train.State == TrainState.Moving)
                {
                    train.CurrentDistanse += train.Speed * Settings.Step;

                    if (train.CurrentSection.Time <= train.CurrentDistanse)
                        EstablishTransFieldsAfterStopping(train);
                    else if (train.Direction == TrainDirection.Direct)
                        train.Coord = YSU_Metro.Utilities.Math.PointCoordsOnVector(
                            train.CurrentSection.From.Coord,
                            train.CurrentSection.To.Coord,
                            train.CurrentDistanse / train.CurrentSection.Time
                        );
                    else
                        train.Coord = YSU_Metro.Utilities.Math.PointCoordsOnVector(
                            train.CurrentSection.To.Coord,
                            train.CurrentSection.From.Coord,
                            train.CurrentDistanse / train.CurrentSection.Time
                        );
                }
                else if (train.State == TrainState.Stopping)
                {
                    train.RemainingTimeStop -= Settings.Step;

                    if (train.RemainingTimeStop <= 0)
                    {
                        if (train.Direction == train.PreviuosTrain.Direction &&
                           ((train.Direction == TrainDirection.Direct && train.PreviuosTrain.CurrentStation == train.CurrentSection.To) ||
                           (train.Direction == TrainDirection.Opposite && train.PreviuosTrain.CurrentStation == train.CurrentSection.From) ||
                           (IsStationFinal(train.CurrentStation) && train.PreviuosTrain.State != TrainState.Moving && train != train.PreviuosTrain && train.CurrentStation == train.PreviuosTrain.CurrentStation))
                           )
                            train.RemainingTimeStop = train.PreviuosTrain.RemainingTimeStop;
                        else if (train.PreviuosTrain.State == TrainState.Moving && train.CurrentSection == train.PreviuosTrain.CurrentSection)
                            train.RemainingTimeStop = train.PreviuosTrain.CurrentDistanse;
                        else
                            EstablishTransFieldsBeforeStarting(train);
                    }
                }
                else
                {
                    if (train.PreviuosTrain.State == TrainState.Moving && train.PreviuosTrain.CurrentSection != train.CurrentSection)
                        EstablishTransFieldsBeforeStarting(train);
                }
            }
        }
    }
}