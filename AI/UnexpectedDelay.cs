﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YSU_Metro.AI
{
    public class UnexpectedDelay
    {
        public string Message { get; set; }
        public int Delay { get; set; }

        public UnexpectedDelay (string message, int delay)
        {
            Message = message;
            Delay = delay;
        }
    }
}
