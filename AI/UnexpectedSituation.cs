﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace YSU_Metro.AI
{
    class UnexpectedSituation : ISituation
    {
        private static Random rnd = new Random();
        private DayOfWeek DayOfWeek;

        public UnexpectedSituation (DayOfWeek DayOfWeek)
        {
            this.DayOfWeek = DayOfWeek;
        }

        public UnexpectedDelay GetUnexpectedDelay(DateTime CurrentTime)
        {
            int probabitily = rnd.Next(1, 11);

            if (DayOfWeek == System.DayOfWeek.Saturday || DayOfWeek == System.DayOfWeek.Sunday ||
                CurrentTime.Hour >= 6 && CurrentTime.Hour <= 18)
            {
                if (probabitily <= 2)
                    return new UnexpectedDelay("", rnd.Next(50, 61));
                else if (probabitily > 2 && probabitily <= 6)
                    return new UnexpectedDelay("", rnd.Next(20, 31));
                else if (probabitily > 6 && probabitily <= 8)
                    return new UnexpectedDelay("", rnd.Next(30, 51));
                else 
                    return new UnexpectedDelay("", 0);
            }
            else
            {
                if (probabitily <= 5)
                    return new UnexpectedDelay("", rnd.Next(50, 61));
                else if (probabitily > 5 && probabitily <= 6)
                    return new UnexpectedDelay("", rnd.Next(20, 31));
                else if (probabitily > 6 && probabitily <= 9)
                    return new UnexpectedDelay("", rnd.Next(30, 51));
                else 
                    return new UnexpectedDelay("", 0);
            }
        }
    }
}
