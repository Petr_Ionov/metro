﻿using System;
using System.Windows.Forms;
using System.Linq;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using YSU_Metro.Models;
using YSU_Metro.Enums;

namespace YSU_Metro.AI
{
    /// <summary>
    /// Настройки моделирования
    /// </summary>
    [Serializable]
    public class ModelingSettings
    {
        private Branch _branch;
        private readonly string _settingsDirectory = Application.StartupPath + "/Settings";

        public DayOfWeek DayOfWeek { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime FinishTime { get; set; }
        public int Step { get; set; }
        public Branch Branch
        {
            get { return _branch; }
            set
            {
                _branch = value;
                LoadSettings();
            }
        }
        public ObservableCollection<Train> Trains { get; set; }

        public ModelingSettings()
        {
            SetDefaultProperties();
        }

        public ModelingSettings(DayOfWeek dayOfWeek, DateTime startTime, DateTime finishTime, 
            int step, Branch branch, ObservableCollection<Train> trains)
        {
            DayOfWeek = dayOfWeek;
            StartTime = startTime;
            FinishTime = finishTime;
            Step = step;
            Branch = branch;
            Trains = trains;
        }

        public void SaveSettings()
        {
            string fileName = string.Format("{0}/{1}.sets", _settingsDirectory, GetBranchUniqueName(_branch));

            if (!Directory.Exists(_settingsDirectory))
            {
                Directory.CreateDirectory(_settingsDirectory);
            }

            try
            {
                using (Stream myStream = File.Open(fileName, FileMode.OpenOrCreate, FileAccess.Write))
                {
                    (new BinaryFormatter()).Serialize(myStream, this);
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Console.WriteLine(ex.Message);
#endif
            }
        }

        private void LoadSettings()
        {
            string fileName = string.Format("{0}/{1}.sets", _settingsDirectory, GetBranchUniqueName(_branch));

            if (!Directory.Exists(_settingsDirectory) || !File.Exists(fileName))
            {
                SetDefaultProperties();
                return;
            }

            try
            {
                using (Stream myStream = File.Open(fileName, FileMode.Open, FileAccess.Read))
                {
                    ModelingSettings settings = (new BinaryFormatter()).Deserialize(myStream) as ModelingSettings;
                    if (settings == null) return;

                    this.StartTime = settings.StartTime;
                    this.FinishTime = settings.FinishTime;
                    this.Step = settings.Step;
                    this.Trains = settings.Trains;
                    this.Branch = settings.Branch;
                    this.DayOfWeek = settings.DayOfWeek;
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Console.WriteLine(ex.Message);
#endif
            }
        }

        private void SetDefaultProperties()
        {
            DayOfWeek = DayOfWeek.Sunday;
            Step = 30;
            StartTime = new DateTime().AddSeconds(21600);
            FinishTime = new DateTime().AddSeconds(86400);
            Trains = new ObservableCollection<Train>();
        }

        private string GetBranchUniqueName(Branch branch)
        {
            int startId = branch.Stations.FindIndex(x => x.State == AvailableState.Working);
            int startStationId = (startId != -1) ? branch.Stations[startId].Id : -1;

            int finishId = branch.Stations.FindLastIndex(x => x.State == AvailableState.Working);
            int finishStationId = (finishId != -1) ? branch.Stations[finishId].Id : -1;

            return Utilities.Security.GetMd5Hash(
                string.Format("{0};;{1};;{2};;{3}", 
                    branch.Id,                                                      
                    branch.Stations.Count(x => x.State == AvailableState.Working),  
                    startStationId,                                                 
                    finishStationId                                                 
                    )
                );
        }
    }
}