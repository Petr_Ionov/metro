﻿using System;
using System.Collections.Generic;
using YSU_Metro.Utilities.Logger;
using YSU_Metro.Models;

namespace YSU_Metro.AI
{
    public class ModelingReport
    {
        public DayOfWeek DayOfWeek { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime FinishTime { get; set; }
        public IEnumerable<LogEntry> LogEntries { get; set; }
        public int Step { get; set; }
        public Branch Branch { get; set; }
        public IEnumerable<Train> Trains { get; set; }
    }
}
