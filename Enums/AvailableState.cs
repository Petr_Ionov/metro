﻿namespace YSU_Metro.Enums
{
    public enum AvailableState
    {
        Working,
        Maintenance,
        Development
    }
}