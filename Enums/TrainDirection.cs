﻿namespace YSU_Metro.Enums
{
    public enum TrainDirection
    {
        Direct,
        Opposite
    }
}