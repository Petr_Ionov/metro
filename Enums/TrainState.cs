﻿namespace YSU_Metro.Enums
{
    public enum TrainState
    {
        Moving,
        Stopping,
        Parking
    }
}