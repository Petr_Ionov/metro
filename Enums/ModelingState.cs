﻿namespace YSU_Metro.Enums
{
    public enum ModelingState
    {
        NotRunning,
        Running,
        Pause
    }
}