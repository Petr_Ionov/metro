﻿using System;
using YSU_Metro.Enums;

namespace YSU_Metro.Models
{
    [Serializable]
    public class Section
    {
        public int Id { get; private set; }
        public int Time { get; private set; }
        public AvailableState State { get; private set; }
        public Station From { get; private set; }
        public Station To { get; private set; }

        public Section(int id, int time, Station from, Station to,
            AvailableState state = AvailableState.Working)
        {
            Id = id;
            Time = time;
            From = from;
            To = to;
            State = state;
        }
    }
}