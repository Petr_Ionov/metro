﻿using System;
using System.Collections.Generic;
using YSU_Metro.Data;

namespace YSU_Metro.Models
{
    public class Metro
    {
        public string Name { get; private set; }
        public string Description { get; private set; }
        public List<Branch> Branches { get; private set; }
        public DateTime TimeFrom { get; private set; }
        public DateTime TimeTo { get; private set; }

        public Metro(string name, string description, List<Branch> branches, DateTime timeFrom, DateTime timeTo)
        {
            Name = name;
            Description = description;
            Branches = branches ?? new List<Branch>();
            TimeFrom = timeFrom;
            TimeTo = timeTo;
        }

        public static Metro Create(IRepository repository)
        {
            try
            {
                return repository.GetMetro();
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
