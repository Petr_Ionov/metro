﻿using System;
using YSU_Metro.Drawing;
using YSU_Metro.Enums;

namespace YSU_Metro.Models
{
    [Serializable]
    public class Station
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public PointD Coord { get; private set; }
        public AvailableState State { get; private set; }
        public TextStyle TextStyle { get; private set; }

        public Station(int id , string name, PointD coord,
            AvailableState state = AvailableState.Working, TextStyle textStyle = null)
        {
            Id = id;
            Name = name;
            Coord = coord;
            State = state;
            TextStyle = textStyle ?? TextStyle.Default;
        }
    }
}