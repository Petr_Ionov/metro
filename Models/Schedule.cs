﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace YSU_Metro.Models
{
    [Serializable]
    public class Schedule
    {
        [Serializable]
        public class Item
        {
            public DayOfWeek DayOfWeek { get; private set; }
            public DateTime From { get; private set; }
            public DateTime To { get; private set; }
            public int Interval { get; private set; }
            public int Stay { get; private set; }

            public Item(DayOfWeek dayOfWeek, DateTime from, DateTime to, int interval, int stay)
            {
                DayOfWeek = dayOfWeek;
                From = from;
                To = to;
                Interval = interval;
                Stay = stay;
            }

            public Item(int dayOfWeek, int from, int to, int interval, int stay)
            {
                DayOfWeek = (DayOfWeek) dayOfWeek;
                From = (new DateTime()).AddSeconds(from);
                To = (new DateTime()).AddSeconds(to);
                Interval = interval;
                Stay = stay;
            }
        }

        private readonly List<Item> _items;

        public Schedule(List<Item> items)
        {
            _items = items ?? new List<Item>();
        }

        /// <summary>
        /// Получить временной интервал между прибытием поездов на станцию
        /// </summary>
        public int GetInterval(DayOfWeek dayOfWeek, DateTime time)
        {
            return GetItem(dayOfWeek, time).Interval;
        }

        public int GetInterval(DayOfWeek dayOfWeek, int time)
        {
            return GetItem(dayOfWeek, (new DateTime()).AddSeconds(time)).Interval;
        }
        /// <summary>
        /// Получить время остановки поезда на станции
        /// </summary>
        public int GetStay(DayOfWeek dayOfWeek, DateTime time)
        {
            return GetItem(dayOfWeek, time).Stay;
        }

        public int GetStay(DayOfWeek dayOfWeek, int time)
        {
            return GetItem(dayOfWeek, (new DateTime()).AddSeconds(time)).Stay;
        }

        private Item GetItem(DayOfWeek dayOfWeek, DateTime time)
        {
            return _items.FirstOrDefault(item => dayOfWeek == item.DayOfWeek && item.From <= time && time <= item.To);
        }
    }
}
