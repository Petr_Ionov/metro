﻿using YSU_Metro.Enums;
using System;
using YSU_Metro.Drawing;

namespace YSU_Metro.Models
{
    [Serializable]
    public class Train
    {
        public int Id { get; private set; }
        public double Speed { get; set; }
        public Branch Branch { get; private set; }
        public Station StartStation { get; set; }
        public TrainDirection Direction { get; set; }
        public TrainState State { get; set; }
        public Train PreviuosTrain { get; set; }
        public Station CurrentStation { get; set; }
        public Section CurrentSection { get; set; }
        public PointD Coord { get; set; }
        public double Angle { get; set; }
        public double CurrentDistanse { get; set; }
        public double RemainingTimeStop { get; set; }

        public Train(int id, double speed, Branch branch, Station startStation, 
            TrainDirection direction = TrainDirection.Direct, TrainState state = TrainState.Parking, 
                PointD coord = new PointD(), double angle = 0)
        {
            Id = id;
            Speed = speed;
            Branch = branch;
            StartStation = startStation;
            Direction = direction;
            State = state;
            Coord = coord;
            Angle = angle;
        }

        public void SetDefaultValues()
        {
            Coord = new PointD(0, 0);
            Angle = 0;
            Speed = 0;
            State = TrainState.Parking;
            CurrentStation = StartStation;
            CurrentSection = null;
        }
    }
}