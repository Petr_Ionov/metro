﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace YSU_Metro.Models
{
    [Serializable]
    public class Branch
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public List<Station> Stations { get; private set; }
        public List<Section> Sections { get; private set; }
        public Color Color { get; private set; }
        public Schedule Schedule { get; private set; }

        public Branch(int id, string name, List<Station> stations, 
            List<Section> sections, Color color, Schedule schedule)
        {
            Id = id;
            Name = name;
            Stations = stations ?? new List<Station>();
            Sections = sections ?? new List<Section>();
            Color = color;
            Schedule = schedule;
        }
    }
}