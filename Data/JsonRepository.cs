﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using YSU_Metro.Models;
using Newtonsoft.Json.Linq;
using YSU_Metro.Enums;
using YSU_Metro.Drawing;

namespace YSU_Metro.Data
{
    public class JsonRepository : IRepository
    {
        public string RepositoryPath { get; protected set; }

        private IEnumerable<Branch> _branches;
        private IEnumerable<Station> _stations;
        private IEnumerable<Section> _sections;

        public JsonRepository(string repositoryPath)
        {
            RepositoryPath = repositoryPath;
        }

        public Metro GetMetro()
        {
            JObject jobject = GetjObject(RepositoryPath + "/metro.json");
            if (jobject == null)
            {
                throw new Exception();
            }

            return new Metro(
                name:           (string)jobject["name"],
                description:    (string)jobject["description"],
                branches:       GetBranches().ToList(),
                timeFrom:       (new DateTime()).AddSeconds((int)jobject["time"]["from"]),
                timeTo:         (new DateTime()).AddSeconds((int)jobject["time"]["to"]));
        }

        public IEnumerable<Branch> GetBranches()
        {
            if (_branches != null) return _branches;

            JArray jarray = GetJArray(RepositoryPath + "/branches.json");
            if (jarray == null)
            {
                throw new Exception();
            }

            List<Branch> result = new List<Branch>();

            foreach (JToken item in jarray)
            {
                List<Station> stations = GetStations().Where(
                    x => item["stations"].Select(i => (int) i).Contains(x.Id)).ToList();
                result.Add(new Branch(
                    id:         (int) item["id"],
                    name:       (string) item["name"],
                    stations:   stations,
                    sections:   GetSections().Where(
                        y => stations.Select(x => x.Id).Contains(y.From.Id) || 
                            stations.Select(x => x.Id).Contains(y.To.Id)).ToList(),
                    color:      System.Drawing.ColorTranslator.FromHtml((string)item["color"]),
                    schedule:   GetSchedule((int) item["id"])));
            }
            _branches = result;

            return result;
        }

        public IEnumerable<Station> GetStations()
        {
            if (_stations != null) return _stations;

            JArray jarray = GetJArray(RepositoryPath + "/stations.json");
            if (jarray == null)
            {
                throw new Exception();
            }

            List<Station> result = jarray.Select(
                item => new Station(
                    id:         (int) item["id"], 
                    name:       (string) item["name"], 
                    coord:      new PointD((double) item["coord"][0], (double) item["coord"][1]), 
                    state:      (AvailableState) ((int) item["state"]), 
                    textStyle:  ParseTextStyle(item))).ToList();

            _stations = result;

            return result;
        }

        public IEnumerable<Section> GetSections()
        {
            if (_sections != null) return _sections;

            JArray jarray = GetJArray(RepositoryPath + "/sections.json");
            if (jarray == null)
            {
                throw new Exception();
            }

            List<Section> result = jarray.Select(
                item => new Section(
                    id:     (int) item["id"], 
                    time:   (int) item["time"], 
                    from:   GetStations().Where(x => x.Id == (int) item["from"]).ToList()[0], 
                    to:     GetStations().Where(x => x.Id == (int) item["to"]).ToList()[0], 
                    state: (AvailableState) ((int) item["state"]))).ToList();

            _sections = result;

            return result;
        }

        public Schedule GetSchedule(int branchId)
        {
            JArray jarray = GetJArray(RepositoryPath + "/schedule.json");
            if (jarray == null)
            {
                throw new Exception();
            }

            List<Schedule.Item> items = new List<Schedule.Item>();

            foreach (JToken item in jarray)
            {
                if ((int) item["branchId"] != branchId) continue;

                foreach (DayOfWeek value in (DayOfWeek[]) Enum.GetValues(typeof (DayOfWeek)))
                {
                    items.AddRange(item["schedule"][((int) value).ToString()]
                        .Select(t => new Schedule.Item(
                            dayOfWeek:  (int) value, 
                            from:       (int) t["from"],
                            to:         (int) t["to"], 
                            interval:   (int) t["interval"], 
                            stay:       (int) t["stay"])));
                }
            }
            return new Schedule(items);
        }

        private string ReadFile(string fileName)
        {
            try
            {
                using (StreamReader reader = new StreamReader(fileName))
                {
                    return reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                Console.WriteLine(ex.Message);
#endif
                MessageBox.Show("Ошибка при получении данных из файла:\n" + fileName, "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return string.Empty;
            }
        }

        private JArray GetJArray(string fileName)
        {
            try
            {
                return JArray.Parse(
                    ReadFile(fileName));
            }
            catch (Exception ex)
            {
#if DEBUG
                Console.WriteLine(ex.Message);
#endif
                MessageBox.Show("Ошибка при получении данных из файла:\n" + fileName, "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private JObject GetjObject(string fileName)
        {
            try
            {
                return JObject.Parse(
                    ReadFile(fileName));
            }
            catch (Exception ex)
            {
#if DEBUG
                Console.WriteLine(ex.Message);
#endif
                MessageBox.Show("Ошибка при получении данных из файла:\n" + fileName, "Ошибка!",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }
        }

        private TextStyle ParseTextStyle(JToken item)
        {
            TextStyle textStyle = TextStyle.Default;

            if (item["style"] == null) return textStyle;
            if (item["style"]["margin"] != null)
            {
                textStyle =
                    new TextStyle(new PointD((double)item["style"]["margin"][0],
                        (double)item["style"]["margin"][1]));
            }
            return textStyle;
        }
    }
}