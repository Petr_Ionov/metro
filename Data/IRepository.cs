﻿using System.Collections.Generic;
using YSU_Metro.Models;

namespace YSU_Metro.Data
{
    public interface IRepository
    {
        Metro GetMetro();
        IEnumerable<Branch> GetBranches();
        IEnumerable<Station> GetStations();
        IEnumerable<Section> GetSections();
        Schedule GetSchedule(int branchId);
    }
}