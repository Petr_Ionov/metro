﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Forms;
using System.Windows.Forms.Integration;
using System.Windows.Media;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using YSU_Metro.AI;
using YSU_Metro.Data;
using YSU_Metro.Drawing;
using YSU_Metro.Models;
using YSU_Metro.Utilities.Logger;
using Application = System.Windows.Forms.Application;
using Brushes = System.Drawing.Brushes;
using Button = System.Windows.Controls.Button;
using Color = System.Windows.Media.Color;
using MessageBox = System.Windows.Forms.MessageBox;

namespace YSU_Metro.Windows
{
    public partial class MainWindow
    {
        private GLControl _glControl;
        private readonly Metro _metro;
        private readonly Modeling _modeling;
        private const int VirtualWidth = 950;
        private const int VirtualHeight = 800;
        private const double AspectRatio = (double)VirtualWidth / VirtualHeight;

        public MainWindow()
        {
            _metro = Metro.Create(new JsonRepository(Application.StartupPath + "/Resources/spb"));
            _modeling = new Modeling();
            _modeling.TrainPositionUpdated += Modeling_TrainPositionUpdated;
            _modeling.ModelingEnded += Modeling_ModelingEnded;
            Logger logger = new Logger();
            logger.OnNewLogEntry += Logger_OnNewLogEntry;
            _modeling.InitLogger(logger);
            InitializeComponent();
        }

        private void Modeling_ModelingEnded()
        {
            _glControl.Invalidate();

            DialogResult dialogResult = MessageBox.Show("Показать отчет?", "Показать отчет?", MessageBoxButtons.YesNo);
            if (dialogResult != System.Windows.Forms.DialogResult.Yes) return;
            ReportWindow report = new ReportWindow(_modeling.GetReport());
            report.ShowDialog();
        }

        private void Logger_OnNewLogEntry(LogEntry logEntry)
        {
            Console.WriteLine(logEntry.ToString("HH:mm:ss"));
        }

        private void Modeling_TrainPositionUpdated()
        {
            if (_glControl != null)
            {
                _glControl.Invalidate();
            }
        }

        private void WindowsFormsHost_Initialized(object sender, EventArgs e)
        {
            _glControl = new GLControl(new GraphicsMode(32, 24), 2, 0, GraphicsContextFlags.Default);
            _glControl.MakeCurrent();
            _glControl.Paint += GLControl_Paint;
            _glControl.VSync = true;
            
            WindowsFormsHost windowsFormsHost = sender as WindowsFormsHost;
            if (windowsFormsHost != null) windowsFormsHost.Child = _glControl;

            GL.Ortho(0, VirtualWidth, VirtualHeight, 0, -1, 1);
            SetViewport(_glControl.Width, _glControl.Height);
        }

        private void GLControl_Paint(object sender, PaintEventArgs e)
        {
            GL.MatrixMode(MatrixMode.Projection);
            GL.LoadIdentity();
            
            GL.ClearColor(Color4.White);
            GL.Clear(
                ClearBufferMask.ColorBufferBit |
                ClearBufferMask.DepthBufferBit |
                ClearBufferMask.StencilBufferBit);

            if (_metro == null)
            {
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                DrawString("Невозможно отобразить метро!", new PointD(250, 370), 
                    new Font(System.Drawing.FontFamily.GenericSansSerif, 20), Brushes.Gray);
                GL.Disable(EnableCap.Blend);
                _glControl.SwapBuffers();
                return;
            }

            foreach (Branch branch in _metro.Branches)
            {
                DrawBranch(branch);
            }

            if (_modeling.Settings.Branch != null)
            {
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                GL.Color4(1, 1, 1, .9);
                GL.Begin(PrimitiveType.Quads);
                GL.Vertex2(0, 0);
                GL.Vertex2(0, VirtualHeight);
                GL.Vertex2(VirtualWidth, VirtualHeight);
                GL.Vertex2(VirtualWidth, 0);
                GL.End();
                GL.Disable(EnableCap.Blend);

                DrawBranch(_modeling.Settings.Branch, true);
            }

            if (_modeling.State != Enums.ModelingState.NotRunning)
            {
                DrawString(string.Format("Время: {0}", _modeling.CurrentTime.ToString("HH:mm:ss")), 
                    new PointD(20, 15), new Font(System.Drawing.FontFamily.GenericSansSerif, 14));
                DrawTrains(_modeling.Settings.Trains);
            }

            _glControl.SwapBuffers();
        }

        private void DrawString(string text, PointD point, Font font = null, System.Drawing.Brush brush = null)
        {
            if (font == null)
            {
                font = new Font(System.Drawing.FontFamily.GenericSansSerif, 13);
            }

            if (brush == null)
            {
                brush = Brushes.Black;
            }

            using (Drawing.TextRenderer textRenderer
                = new Drawing.TextRenderer(VirtualWidth, VirtualHeight))
            {
                textRenderer.DrawString(text, font, brush, point);

                GL.Enable(EnableCap.Blend);
                GL.Enable(EnableCap.Texture2D);
                GL.BindTexture(TextureTarget.Texture2D, textRenderer.Texture);
                GL.Begin(PrimitiveType.Quads);
                GL.TexCoord2(0, 0);
                GL.Vertex2(0, 0);
                GL.TexCoord2(1, 0);
                GL.Vertex2(VirtualWidth, 0);
                GL.TexCoord2(1, 1);
                GL.Vertex2(VirtualWidth, VirtualHeight);
                GL.TexCoord2(0, 1);
                GL.Vertex2(0, VirtualHeight);
                GL.End();
                GL.Disable(EnableCap.Texture2D);
                GL.Disable(EnableCap.Blend);
            }
        }

        private void WindowsFormsHost_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            SetViewport(_glControl.Width, _glControl.Height);
        }

        private void DrawBranch(Branch branch, bool drawStationNames = false)
        {
            GL.Color4(branch.Color);

            GL.LineWidth(3);
            //Сглаживание
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
            GL.Enable(EnableCap.Blend);
            // Сглаживание линий
            GL.Enable(EnableCap.LineSmooth);
            GL.Hint(HintTarget.LineSmoothHint, HintMode.Nicest);
            foreach (Section section in branch.Sections)
            {
                if (section.State != Enums.AvailableState.Working)
                {
                    GL.Enable(EnableCap.LineStipple);
                    GL.LineStipple((int)(Utilities.Math.VectorLength(
                        section.From.Coord, section.To.Coord) / 8), 500);
                }
                GL.Begin(PrimitiveType.Lines);
                GL.Vertex2(section.From.Coord.X, section.From.Coord.Y);
                GL.Vertex2(section.To.Coord.X, section.To.Coord.Y);
                GL.End();

                GL.Disable(EnableCap.LineStipple);
            }
            GL.Disable(EnableCap.Blend);
            GL.Disable(EnableCap.LineSmooth);

            foreach (Station station in branch.Stations)
            {
                DrawCircle(station.Coord.X, station.Coord.Y, 8);
                if (station.State != Enums.AvailableState.Working)
                {
                    GL.Color4(Color4.White);
                    DrawCircle(station.Coord.X, station.Coord.Y, 4);
                    GL.Color4(branch.Color);
                }

                if (drawStationNames)
                {
                    DrawString(station.Name, station.Coord + station.TextStyle.Margin);
                }
            }
        }

        private void SetViewport(int width, int height)
        {
            double aspectRatio = (double)width / height;
            double scale;
            Vector2d crop = new Vector2d(0d, 0d);

            if (aspectRatio > AspectRatio)
            {
                scale = (double)height / VirtualHeight;
                crop.X = (width - VirtualWidth * scale) / 2d;
            }
            else if (aspectRatio < AspectRatio)
            {
                scale = (double)width / VirtualWidth;
                crop.Y = (height - VirtualHeight * scale) / 2d;
            }
            else
            {
                scale = (double)width / VirtualWidth;
            }

            int w = (int)(VirtualWidth * scale);
            int h = (int)(VirtualHeight * scale);
            GL.Viewport((int)crop.X, (int)crop.Y, w, h);
        }

        private void DrawCircle(double cx, double cy, double r)
        {
            GL.Enable(EnableCap.PointSmooth);
            GL.Hint(HintTarget.PointSmoothHint, HintMode.Nicest);
            GL.Begin(PrimitiveType.TriangleFan);
            for (int i = 0; i < 360; i++)
            {
                double theta = i * Math.PI / 180;
                GL.Vertex2(r * Math.Cos(theta) + cx, r * Math.Sin(theta) + cy);
            }
            GL.End();
        }

        private void DrawTrains(IEnumerable<Train> trains)
        {
            foreach (Train train in trains)
            {
                GL.Color4(train.Direction == Enums.TrainDirection.Direct ? Color4.Pink : Color4.Blue);
                DrawCircle(train.Coord.X, train.Coord.Y, 10);
            }
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            if (_metro == null)
            {
                SidebarBranches.Visibility = Visibility.Hidden;
                SidebarButtons.Visibility = Visibility.Hidden;
                return;
            }

            foreach (Branch branch in _metro.Branches)
            {
                Button btn = new Button
                {
                    Content = branch.Name,
                    Style = FindResource("BranchButtonStyle") as Style
                };

                btn.Resources["BranchId"] = branch.Id.ToString();
                btn.Resources["BranchColor"] =
                    new SolidColorBrush(Color.FromArgb(branch.Color.A, branch.Color.R, branch.Color.G,
                        branch.Color.B));

                btn.Click += (s, ev) =>
                {
                    if (_modeling.State != Enums.ModelingState.NotRunning) return;
                    _modeling.Settings.Branch = branch;
                    foreach (Button button in BranchesPanel.Children.OfType<Button>())
                    {
                        button.IsEnabled = !button.Equals(s as Button);
                    }

                    if (SidebarButtons.Visibility != Visibility.Visible)
                    {
                        SidebarError.Visibility = Visibility.Collapsed;
                        SidebarButtons.Visibility = Visibility.Visible;
                    }

                    _glControl.Invalidate();
                };
                BranchesPanel.Children.Add(btn);
            }
        }

        private void BtnSettings_Click(object sender, RoutedEventArgs e)
        {
            if (_modeling.Settings.Branch == null) return;

            if (_modeling.Settings.Branch.Stations.Count(x => x.State == Enums.AvailableState.Working) < 2)
            {
                MessageBox.Show("На выбранной ветке нет 2 или более рабочих станций!", "Невозможно открыть настройки", MessageBoxButtons.OK);
                return;
            }

            SettingsWindow settings = new SettingsWindow(_modeling.Settings);
            settings.ShowDialog();
        }

        private void BtnStart_Click(object sender, RoutedEventArgs e)
        {
            if (_modeling.Settings.Branch == null 
                || _modeling.State == Enums.ModelingState.Running) return;

            if (_modeling.Settings.Trains.Count < 1)
            {
                MessageBox.Show("На ветке нет ни одного поезда!", "Невозможно запустить моделирование", MessageBoxButtons.OK);
                return;
            }

            _modeling.Start();

            StartButton.Visibility = Visibility.Collapsed;
            ResumeButton.Visibility = Visibility.Collapsed;
            SettingsButton.Visibility = Visibility.Collapsed;
            PauseButton.Visibility = Visibility.Visible;
            StopButton.Visibility = Visibility.Visible;
        }

        private void BtnPause_Click(object sender, RoutedEventArgs e)
        {
            _modeling.Pause();

            PauseButton.Visibility = Visibility.Collapsed;
            ResumeButton.Visibility = Visibility.Visible;
        }

        private void BtnStop_Click(object sender, RoutedEventArgs e)
        {
            _modeling.Stop();

            StartButton.Visibility = Visibility.Visible;
            ResumeButton.Visibility = Visibility.Collapsed;
            SettingsButton.Visibility = Visibility.Visible;
            PauseButton.Visibility = Visibility.Collapsed;
            StopButton.Visibility = Visibility.Collapsed;
        }

        private void BtnResume_Click(object sender, RoutedEventArgs e)
        {
            _modeling.Resume();

            ResumeButton.Visibility = Visibility.Collapsed;
            PauseButton.Visibility = Visibility.Visible;
        }
    }
}
