﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using YSU_Metro.AI;
using YSU_Metro.Models;
using YSU_Metro.UI.DateRangeSlider;

namespace YSU_Metro.Windows
{
    public partial class SettingsWindow
    {
        private readonly ModelingSettings _settings;

        public SettingsWindow(ModelingSettings settings)
        {
            _settings = settings; //Нарушает ООП
            InitializeComponent();
            Title = string.Format("Настройки моделирования на ветке {0}", settings.Branch.Name);
            LoadParams();
        }

        private void LoadParams()
        {
            PeriodRangeSlider.Minimum = (new DateTime()).AddSeconds(21600);
            PeriodRangeSlider.Maximum = (new DateTime()).AddSeconds(86400);

            PeriodRangeSlider.LowerValue = (_settings.StartTime > DateTime.MinValue) 
                ? _settings.StartTime : PeriodRangeSlider.Minimum;

            PeriodRangeSlider.UpperValue = (_settings.FinishTime > DateTime.MinValue)
                ? _settings.FinishTime : PeriodRangeSlider.Maximum;

            PeriodRangeSlider.LowerValueChanged += PeriodRangeSlider_LowerValueChanged;
            PeriodRangeSlider.UpperValueChanged += PeriodRangeSlider_UpperValueChanged;

            StepSlider.Value = (_settings.Step >= StepSlider.Minimum && _settings.Step <= StepSlider.Maximum)
                ? _settings.Step : StepSlider.Minimum;
            StepSlider.ValueChanged += StepSlider_ValueChanged;


            int startStationId = _settings.Branch.Stations.FindIndex(x => x.State == Enums.AvailableState.Working);
            if (startStationId != -1)
            {
                Station startStation = _settings.Branch.Stations[startStationId];
                StartStation.Tag = startStation.Id;
                StartStation.Content = startStation.Name;
            }

            int endStationId = _settings.Branch.Stations.FindLastIndex(x => x.State == Enums.AvailableState.Working);
            if (endStationId != -1)
            {
                Station endStation = _settings.Branch.Stations[endStationId];
                FinishStation.Tag = endStation.Id;
                FinishStation.Content = endStation.Name;
            }

            foreach (Button btn in DoWPanel.Children.OfType<Button>())
            {
                if ((DayOfWeek) btn.Tag != _settings.DayOfWeek) continue;
                btn.IsEnabled = false;
                break;
            }

            TrainsList.ItemsSource = _settings.Trains;
            StartStation.IsEnabled = false;
        }

        private void StepSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            if (slider != null) _settings.Step = (int)slider.Value;
        }

        private void DayButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null) return;

            _settings.DayOfWeek = (DayOfWeek) button.Tag;

            foreach (Button btn in DoWPanel.Children.OfType<Button>())
            {
                btn.IsEnabled = !btn.Equals(button);
            }
        }

        private void PeriodRangeSlider_LowerValueChanged(object sender, RoutedEventArgs e)
        {
            DateRangeSlider dateRangeSlider = sender as DateRangeSlider;
            if (dateRangeSlider != null) _settings.StartTime = dateRangeSlider.LowerValue;
        }

        private void PeriodRangeSlider_UpperValueChanged(object sender, RoutedEventArgs e)
        {
            DateRangeSlider dateRangeSlider = sender as DateRangeSlider;
            if (dateRangeSlider != null) _settings.FinishTime = dateRangeSlider.UpperValue;
        }

        private void AddTrain_Click(object sender, RoutedEventArgs e)
        {
            int id = -1;
            if (NumText.Text.Length < 1 || !int.TryParse(NumText.Text, out id))
            {
                //Не указан или неверно введен id
            }
            else
            {
                if (_settings.Trains.Count(x => x.Id == id) > 0)
                {
                    //Уже есть с таким номеров
                    return;
                }
                int stationId = (!StartStation.IsEnabled) ? int.Parse(StartStation.Tag.ToString()) : int.Parse(FinishStation.Tag.ToString());
                Station station = _settings.Branch.Stations.Find(x => x.Id == stationId);
                _settings.Trains.Add(
                    new Train(id, 1d, _settings.Branch, station));
                TrainsList.UnselectAll();
            }
        }

        private void DeleteTrain_Click(object sender, RoutedEventArgs e)
        {
            if (TrainsList.SelectedIndex != -1)
            {
                _settings.Trains.RemoveAt(TrainsList.SelectedIndex);
            }
        }

        private void StationButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (Button btn in StationSelector.Children.OfType<Button>())
            {
                btn.IsEnabled = !btn.Equals(sender as Button);
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _settings.SaveSettings();
        }

        private void TrainsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListView listView = sender as ListView;
            DeleteTrain.IsEnabled = listView != null && listView.SelectedIndex != -1;
        }

        private void NumText_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            int val;
            if (!int.TryParse(e.Text, out val))
            {
                e.Handled = true;
            }
        }
    }
}