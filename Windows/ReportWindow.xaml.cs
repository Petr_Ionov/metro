﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using YSU_Metro.AI;
using YSU_Metro.Utilities.Logger;

namespace YSU_Metro.Windows
{
    public partial class ReportWindow
    {
        private readonly ModelingReport _modelingReport;
        public ReportWindow(ModelingReport report)
        {
            _modelingReport = report;
            InitializeComponent();
            LoadReport();
        }

        private void LoadReport()
        {
            BranchNameTxt.Text = _modelingReport.Branch.Name;
            DayOfWeekTxt.Text = GetNameDayOfWeek(_modelingReport.DayOfWeek);
            StartTimeTxt.Text = _modelingReport.StartTime.ToString("HH:mm");
            FinishTimeTxt.Text = _modelingReport.FinishTime.ToString("HH:mm");
            StepTxt.Text = _modelingReport.Step.ToString();
            CountTrainsTxt.Text = _modelingReport.Trains.Count().ToString();
            TrainsList.ItemsSource = _modelingReport.Trains;
            foreach (LogEntry logEntry in _modelingReport.LogEntries)
            {
                LogsRtb.AppendText(logEntry.ToString("HH:mm:ss") + "\r");
            }
        }

        private string GetNameDayOfWeek(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Monday:
                    return "Понедельник";
                case DayOfWeek.Tuesday:
                    return "Вторник";
                case DayOfWeek.Wednesday:
                    return "Среда";
                case DayOfWeek.Thursday:
                    return "Четверг";
                case DayOfWeek.Friday:
                    return "Пятница";
                case DayOfWeek.Saturday:
                    return "Суббота";
                case DayOfWeek.Sunday:
                    return "Воскресенье";
                default:
                    return "";
            }
        }
    }
}
