﻿using System;
using System.Windows.Data;

namespace YSU_Metro.UI.Converters
{
    [ValueConversion(typeof(DateTime), typeof(int))]
    public class DateTimeIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return (int)DateTime.Parse(value.ToString())
                .Subtract(DateTime.MinValue).TotalMinutes;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return new DateTime().AddMinutes(
                int.Parse(value.ToString())
                );
        }
    }
}
