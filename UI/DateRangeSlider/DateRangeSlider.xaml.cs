﻿using System;
using System.Windows;

namespace YSU_Metro.UI.DateRangeSlider
{
    public partial class DateRangeSlider
    {
        private const string StrMask = "HH:mm";

        public event RoutedEventHandler LowerValueChanged;
        public event RoutedEventHandler UpperValueChanged;

        public DateTime Minimum
        {
            get { return (DateTime)GetValue(MinimumProperty); }
            set { SetValue(MinimumProperty, value); }
        }

        public DateTime Maximum
        {
            get { return (DateTime)GetValue(MaximumProperty); }
            set { SetValue(MaximumProperty, value); }
        }

        public DateTime LowerValue
        {
            get { return (DateTime)GetValue(LowerValueProperty); }
            set
            {
                SetValue(LowerValueProperty, value);
                StrLowerValue = value.ToString(StrMask);
            }
        }

        public DateTime UpperValue
        {
            get { return (DateTime)GetValue(UpperValueProperty); }
            set
            {
                SetValue(UpperValueProperty, value);
                StrUpperValue = value.ToString(StrMask);
            }
        }

        public string StrLowerValue
        {
            get { return LowerValue.ToString(StrMask); }
            set { SetValue(StrLowerValueProperty, value); }
        }

        public string StrUpperValue
        {
            get { return UpperValue.ToString(StrMask); }
            set { SetValue(StrUpperValueProperty, value); }
        }

        public int MinRange
        {
            get { return (int)GetValue(MinRangeProperty); }
            set { SetValue(MinRangeProperty, value); }
        }

        public static readonly DependencyProperty MinimumProperty =
            DependencyProperty.Register("Minimum", typeof(DateTime), 
                typeof(DateRangeSlider), new UIPropertyMetadata(DateTime.Now.AddDays(-15)));

        public static readonly DependencyProperty MaximumProperty =
            DependencyProperty.Register("Maximum", typeof(DateTime), 
                typeof(DateRangeSlider), new UIPropertyMetadata(DateTime.Now.AddDays(15)));

        public static readonly DependencyProperty LowerValueProperty =
            DependencyProperty.Register("LowerValue", typeof(DateTime), 
                typeof(DateRangeSlider), new UIPropertyMetadata(DateTime.Now.AddDays(-7)));

        public static readonly DependencyProperty UpperValueProperty =
            DependencyProperty.Register("UpperValue", typeof(DateTime), 
                typeof(DateRangeSlider), new UIPropertyMetadata(DateTime.Now.AddDays(7)));

        public static readonly DependencyProperty StrLowerValueProperty =
            DependencyProperty.Register("StrLowerValue", typeof (string),
                typeof (DateRangeSlider), new UIPropertyMetadata(DateTime.Now.AddDays(-7).ToString(StrMask)));

        public static readonly DependencyProperty StrUpperValueProperty =
            DependencyProperty.Register("StrUpperValue", typeof (string),
                typeof (DateRangeSlider), new UIPropertyMetadata(DateTime.Now.AddDays(7).ToString(StrMask)));

        public static readonly DependencyProperty MinRangeProperty =
            DependencyProperty.Register("MinRange", typeof (int),
                typeof (DateRangeSlider), new UIPropertyMetadata(30));

        public DateRangeSlider()
        {
            InitializeComponent();
            Loaded += Slider_Loaded;
        }

        protected void Slider_Loaded(object sender, RoutedEventArgs e)
        {
            LowerSlider.ValueChanged += LowerSlider_ValueChanged;
            UpperSlider.ValueChanged += UpperSlider_ValueChanged;
        }

        private void LowerSlider_ValueChanged(object sender, 
            RoutedPropertyChangedEventArgs<double> e)
        {
            UpperSlider.Value = Math.Max(UpperSlider.Value, LowerSlider.Value + MinRange);

            DateTime upperValue = new DateTime().AddMinutes(Math.Round(UpperSlider.Value));
            DateTime lowerValue = (UpperSlider.Value - MinRange < LowerSlider.Value) ?
                new DateTime().AddMinutes(Math.Round(UpperSlider.Value - MinRange)) : 
                new DateTime().AddMinutes(Math.Round(LowerSlider.Value));

            if (UpperValue > lowerValue)
            {
                LowerValue = lowerValue;
                UpperValue = upperValue;
            }
            else
            {
                UpperValue = upperValue;
                LowerValue = lowerValue;
            }

            if (LowerValueChanged != null)
                LowerValueChanged(this, e);
        }

        private void UpperSlider_ValueChanged(object sender, 
            RoutedPropertyChangedEventArgs<double> e)
        {
            LowerSlider.Value = Math.Max(Math.Min(UpperSlider.Value - MinRange, 
                LowerSlider.Value), Minimum.Subtract(DateTime.MinValue).TotalMinutes);

            DateTime upperValue = (LowerSlider.Value + MinRange > UpperSlider.Value) ?
                new DateTime().AddMinutes(Math.Round(LowerSlider.Value + MinRange)) :
                new DateTime().AddMinutes(Math.Round(UpperSlider.Value));
            DateTime lowerValue = new DateTime().AddMinutes(Math.Round(LowerSlider.Value));

            if (UpperValue > lowerValue)
            {
                LowerValue = lowerValue;
                UpperValue = upperValue;
            }
            else
            {
                UpperValue = upperValue;
                LowerValue = lowerValue;
            }

            if (UpperValueChanged != null)
                UpperValueChanged(this, e);
        }
    }
}